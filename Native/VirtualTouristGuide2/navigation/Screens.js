import React from "react";
import { Easing, Animated } from "react-native";

import {createAppContainer} from 'react-navigation';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack';

import { Block } from "galio-framework";

// screens
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import HomeScreen from '../screens/HomeScreen';
import NearbyPlaces from '../screens/NearbyPlaces'
import CameraScreen from '../screens/CameraScreen';
import BuildItineraryScreen from '../screens/BuildItineraryScreen';
import IndoorNavigationScreen from '../screens/IndoorNavigationScreen';
import NearbyPlacesScreen from '../screens/NearbyPlacesScreen';
import SuggestionsScreen from '../screens/SuggestionsScreen';
import ChatBotScreen from '../screens/ChatBotScreen';
import ChatForumScreen from '../screens/ChatForumScreen';
import CompanionScreen from '../screens/CompanionScreen';
import SecurityScreen from '../screens/SecurityScreen';
// drawer
import Menu from "./Menu";
import DrawerItem from "../components/DrawerItem";

// header for screens
import Header from "../components/Header";

const transitionConfig = (transitionProps, prevTransitionProps) => ({
  transitionSpec: {
    duration: 400,
    easing: Easing.out(Easing.poly(4)),
    timing: Animated.timing
  },
  screenInterpolator: sceneProps => {
    const { layout, position, scene } = sceneProps;
    const thisSceneIndex = scene.index;
    const width = layout.initWidth;

    const scale = position.interpolate({
      inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
      outputRange: [4, 1, 1]
    });
    const opacity = position.interpolate({
      inputRange: [thisSceneIndex - 1, thisSceneIndex, thisSceneIndex + 1],
      outputRange: [0, 1, 1]
    });
    const translateX = position.interpolate({
      inputRange: [thisSceneIndex - 1, thisSceneIndex],
      outputRange: [width, 0]
    });

    const scaleWithOpacity = { opacity };
    const screenName = "Search";

    if (
      screenName === transitionProps.scene.route.routeName ||
      (prevTransitionProps &&
        screenName === prevTransitionProps.scene.route.routeName)
    ) {
      return scaleWithOpacity;
    }
    return { transform: [{ translateX }] };
  }
});

const SettingsScreenStack = createStackNavigator({
  SettingsScreen: {
    screen: SettingsScreen,
    navigationOptions: ({ navigation }) => ({
      
      header: <Header title="Home "  barStyle= 'dark-content'  navigation={navigation} />,
      
        
    
      
    })
  }
},{
  cardStyle: {
    backgroundColor: ""
  },
  transitionConfig
});

const BuildItineraryScreenStack = createStackNavigator({
  BuildItineraryScreen: {
    screen: BuildItineraryScreen,
    navigationOptions: ({ navigation }) => ({
      header: <Header title="Build Itinerary"   navigation={navigation} />,
      
    })
  }
},{
  cardStyle: {
    backgroundColor: "#F8F9FE"
  },
  transitionConfig
});

const CameraScreenStack = createStackNavigator(
  {
    CameraScreen: {
      screen: CameraScreen,
      navigationOptions: ({ navigation }) => ({
        header: <Header title="Capture"   navigation={navigation} />,
        headerTransparent: true
      })
    }
  },
  {
    cardStyle: { backgroundColor: "#FFFFFF" },
    transitionConfig
  }
);

const IndoorNavigationScreenStack = createStackNavigator(
  {
    IndoorNavigationScreen: {
      screen: IndoorNavigationScreen,
      navigationOptions: ({ navigation }) => ({
       
        header: 
          <Header  title="Indoor Navigation" iconColor={''}  navigation={navigation} />,
        
        headerTransparent: true
      })
    }
  },
  {
    cardStyle: { backgroundColor: "#FFFFFF" },
    transitionConfig
  }
);

const NearbyPlacesScreenStack = createStackNavigator(
  {
    NearbyPlacesScreen: {
      screen: NearbyPlacesScreen,
      navigationOptions: ({ navigation }) => ({
        header: 
          <Header  title="Nearby Places" iconColor={''} navigation={navigation} />
        ,
        headerTransparent: true
      })
    }
  },
  {
    cardStyle: { backgroundColor: "#FFFFFF" },
    transitionConfig
  }
);

const SuggestionsScreenStack = createStackNavigator(
  {
    SuggestionsScreen: {
      screen: SuggestionsScreen,
      navigationOptions: ({ navigation }) => ({
        header: 
          <Header  title="Suggestions" iconColor={''} navigation={navigation} />
        ,
        headerTransparent: true
      })
    }
  },
  {
    cardStyle: { backgroundColor: "#FFFFFF" },
    transitionConfig
  }
);

const ChatBotScreenStack = createStackNavigator(
  {
    ChatBotScreen: {
      screen: ChatBotScreen,
      navigationOptions: ({ navigation }) => ({
        header: 
          <Header title="ChatBot" iconColor={''} navigation={navigation} />
        ,
        headerTransparent: true
      })
    }
  },
  {
    cardStyle: { backgroundColor: "#FFFFFF" },
    transitionConfig
  }
);

const ChatForumScreenStack = createStackNavigator(
  {
    ChatForumScreen: {
      screen: ChatForumScreen,
      navigationOptions: ({ navigation }) => ({
        header: 
          <Header title="ChatForum" iconColor={''} navigation={navigation} />
        ,
        headerTransparent: true
      })
    }
  },
  {
    cardStyle: { backgroundColor: "#FFFFFF" },
    transitionConfig
  }
);

const CompanionScreenStack = createStackNavigator(
  {
    CompanionScreen: {
      screen: CompanionScreen,
      navigationOptions: ({ navigation }) => ({
        header: (
          <Header  title="Companion" iconColor={''} navigation={navigation} />
        ),
        headerTransparent: true
      })
    }
  },
  {
    cardStyle: { backgroundColor: "#FFFFFF" },
    transitionConfig
  }
);

const SecurityScreenStack = createStackNavigator(
  {
    SecurityScreen: {
      screen: SecurityScreen,
      navigationOptions: ({ navigation }) => ({
        header: 
          <Header  title="Security" iconColor={''} navigation={navigation} />
        ,
        headerTransparent: true
      })
    }
  },
  {
    cardStyle: { backgroundColor: "#FFFFFF" },
    transitionConfig
  }
);

// divideru se baga ca si cum ar fi un ecrna dar nu-i nimic duh
const AppStack = createDrawerNavigator(
  {
    // Onboarding: {
    //   screen: Onboarding,
    //   navigationOptions: {
    //     drawerLabel: () => {}
    //   }
    // },
    SettingsScreen: {
      screen: SettingsScreenStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} screen="SettingsScreen" title="Home" />
        )
      })
    },
    BuildItineraryScreen: {
      screen: BuildItineraryScreenStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} screen="BuildItineraryScreen" title="Build Itinerary" />
        )
      })
    },
    CameraScreen: {
      screen: CameraScreenStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} screen="CameraScreenStack" title="Capture" />
        )
      })
    },
    IndoorNavigationScreen: {
      screen: IndoorNavigationScreenStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} screen="IndoorNavigationScreen" title="Indoor Navigation" />
        )
      })
    },
    NearbyPlacesScreen: {
      screen: NearbyPlacesScreenStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} screen="NearbyPlacesScreen" title="Nearby Places" />
        )
      })
    },
    SuggestionsScreen: {
      screen: SuggestionsScreenStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} screen="SuggestionsScreen" title="Suggestions" />
        )
      })
    },
    
      
    ChatBotScreen: {
      screen: ChatBotScreenStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} screen="ChatBotScreen" title="ChatBot" />
        )
      })
    },
    ChatForumScreen: {
      screen: ChatForumScreenStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} screen="ChatForumScreen" title="ChatForum" />
        )
      })
    },
    CompanionScreen: {
      screen: CompanionScreenStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} screen="CompanionScreen" title="Companion" />
        )
      })
    },
    SecurityScreen: {
      screen: SecurityScreenStack,
      navigationOptions: navOpt => ({
        drawerLabel: ({ focused }) => (
          <DrawerItem focused={focused} screen="SecurityScreen" title="Security" />
        )
      })
    },
  },
  Menu
);

const AppContainer = createAppContainer(AppStack);
export default AppContainer;
