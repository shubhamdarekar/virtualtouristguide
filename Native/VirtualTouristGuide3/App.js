import React, {useState, useEffect} from 'react';
import { StyleSheet, AsyncStorage} from 'react-native';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';
import * as firebase from 'firebase';
import { useKeepAwake } from 'expo-keep-awake';



import ApiKeys from './constants/ApiKeys';
import { Ionicons } from '@expo/vector-icons';
import Providers from './constants/Providers';

export default function App(props) {

  const [isLoadingComplete, setLoadingComplete] = useState(false);

  useEffect(()=>{
    //init firebase
    if (!firebase.apps.length)
    { 
      firebase.initializeApp(ApiKeys.FirebaseConfig);  
    };
    // AsyncStorage.removeItem('isFirstTime');
    // AsyncStorage.removeItem('user');
  },[])
  useKeepAwake();

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return (    
    <Providers/>
    );
  }

}


  //loading resources
  async function loadResourcesAsync() {
    await Promise.all([
      Asset.loadAsync([
        //add images to load
        // require('./assets/images/robot-dev.png'),
        require('./assets/images/logo.png'),
      ]),
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Ionicons.font,
        // We include SpaceMono because we use it in HomeScreen.js. Feel free to
        // remove this if you are not using it in your app
        'pacifico':require('./assets/fonts/Pacifico.ttf'),
        'karla':require('./assets/fonts/Karla-Regular.ttf'),
      }),
    ]);
  }

  //handling error
  function handleLoadingError(error) {
    // In this case, you might want to report the error to your error reporting
    // service, for example Sentry
    console.warn(error);
  }

  //finish loading
  function handleFinishLoading(setLoadingComplete) {
    setLoadingComplete(true);
  }


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
