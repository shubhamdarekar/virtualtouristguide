import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  ImageBackground,
  Image,
  TextInput,
  Dimensions,
  KeyboardAvoidingView,
  TouchableOpacity
} from 'react-native';

import AwesomeButtonRick from 'react-native-really-awesome-button/src/themes/rick';
import bgSrc from '../../assets/images/wallpaper.png';
import logoImg from '../../assets/images/logo.png';
import eyeImg from '../../assets/images/eye_black.png';
import {AuthContext} from '../../constants/AuthProvider'
import LottieView from 'lottie-react-native';

export default class EnterPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      password:'',
      showPass: true,
    };
    this.showPass = this.showPass.bind(this);
  }

  static contextType = AuthContext;
  componentDidMount(){
    const auth = this.context;
  }

  componentWillUnmount(){
    if(this.timer){
      clearTimeout(this.timer);
      this.timer = 0;
    }
  }

  showPass() {
    this.state.press === false
      ? this.setState({showPass: false, press: true})
      : this.setState({showPass: true, press: false});
  }

  render() {
    return (
        <ImageBackground style={styles.picture} source={bgSrc}>
          <LottieView style={styles.animation}
        ref={animation => {
          this.animation = animation;
        }}
        source={require('../../assets/1728-swipe-left-to-right.json')}
      />
        <View style={styles.logoview}>
        
          <Image source={logoImg} style={styles.logoimage} />
          <Text style={styles.nametext}>TRAVELr</Text>
        </View>
        <KeyboardAvoidingView behavior={'padding'} style={styles.centerWrapper}>
        <View style={styles.centerWrapper}>
        <Text style= {styles.welcomeText}>Welcome back {this.props.route.params.number}, Enter Your Password :</Text>
          <View style={styles.inputWrapper}>
            <TextInput
              style={styles.input}
              secureTextEntry={this.state.showPass}
              placeholder={'Password'}
              keyboardAppearance={'dark'}
              autoCorrect={false}
              autoCapitalize={'none'}
              returnKeyType={'done'}
              placeholderTextColor="white"
              underlineColorAndroid="transparent"
              autoFocus={true}
              onChangeText={text => {
                this.setState({ password: text });
              }}
              value={this.state.password}
            />
            <TouchableOpacity
          activeOpacity={0.5}
          style={styles.btnEye}
          onPress={this.showPass}>
          <Image source={eyeImg} style={styles.iconEye} />
        </TouchableOpacity>
          </View>

          <View style={styles.nextWrapper}>
            <AwesomeButtonRick
              type="secondary"
              progress
              width={150}
              raiseLevel={10}
              onPress={next => {
                // console.log(this.props.route.params.number+this.state.password)
                this.context.login(this.props.route.params.number,this.state.password);
                next();
              }}
              >
              Login
            </AwesomeButtonRick>
            <TouchableOpacity style= {styles.wrongUser} 
              onPress={() => {
                this.animation.play();
                this.timer = setTimeout(()=>{
                  this.animation.reset();
                  this.timer = 0;
                }, 3000);
              }}
            >
              <Text style= {styles.wrongUserText}>Not {this.props.route.params.number}?</Text>
            </TouchableOpacity>
          </View>
        </View>
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 0,
  },
  picture: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  logoview: {
    top: DEVICE_HEIGHT / 10 - 20,
    flex: 0.7,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoimage: {
    width: 80,
    height: 80,
  },
  nametext: {
    color: 'white',
    backgroundColor: 'transparent',
    marginTop: 20,
    fontFamily: 'pacifico',
    fontSize: 30,
  },
  centerWrapper: {
    flex: 1,
    flexDirection: 'column',
  },
  welcomeText:{
    color:'#22dd6d',
    fontSize:20,
    fontFamily:'karla',
    bottom:-65,
    textAlign:'center',
  },
  inputWrapper: {
    top: 70,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  input: {
    backgroundColor: 'rgba(255, 255, 255, 0.6)',
    width: 350,
    height: 47,
    marginHorizontal: (DEVICE_WIDTH - 350) / 2,
    paddingLeft: 10,
    paddingRight: 30,
    borderRadius: 30,
    color: '#ffffff',
    position: 'absolute',
    textAlign: 'center',
  },
  nextWrapper: {
    top: 15,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  btnEye: {
    position: 'absolute',
    top: 10,
    right: 68,
  },
  iconEye: {
    width: 25,
    height: 25,
    tintColor: 'rgba(0,0,0,0.7)',
  },
  wrongUser:{
    paddingTop:25,
    
  },
  wrongUserText:{
    color:'#660421',
    fontSize:20,
    fontFamily:'karla',
  },
  animation:{
    width:300,
    height:150,
    top:0,
    position: 'absolute',
  }
});