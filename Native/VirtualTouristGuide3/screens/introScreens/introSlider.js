import React from 'react';
import { Ionicons } from '@expo/vector-icons';
import { StyleSheet, View, TouchableOpacity, AsyncStorage,Vibration} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import {FtimeContext} from '../../constants/FtimeProvider'

const styles = StyleSheet.create({
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    
  },
  image: {
    width: 320,
    height: 320,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  pagestyle:{

  }
});

const slides = [
  {
    key: 'somethun',
    title: ' Welcome to\nTRAVELr',
    text: "Let's Get Started  \n\nTake control of your travel \nby tracking and constantly updating\n your itinerary, ",
    backgroundColor: '#5959AB',
    image :require( '../../assets/images/logo.png'),
    justifyContent: 'space-around',
    titleStyle : {
      color:'#ffffff',
      fontSize: 50,
      
      textShadowColor:'#FFFFFF',
      textShadowRadius:30,
      fontFamily:'pacifico',
      textAlign:'center',
    },
    textStyle:{
      color:'#ffffff',
      textShadowColor:'#FFFFFF',
      fontSize: 20,
      textShadowRadius:10,
      fontWeight:'bold',
      fontFamily:'karla',
      textAlign:'center'
    }
  },
  {
    key: 'somethun-dos',
    title: 'Instant Information',
    text: 'Get instant information about your\n place you are visiting just by clicking a picture',
    backgroundColor: '#009ACD',
    image :require( '../../assets/images/slider/slide2.png'),
    justifyContent: 'space-around',
    titleStyle : {
      color:'#FFFFFF',
      fontSize: 50,
      fontWeight:'bold',
      textShadowColor:'#FFFFFF',
      textShadowRadius:30,
      fontFamily:'karla',
      textAlign:'center',
    },
    textStyle:{
      color:'#ffffff',
      textShadowColor:'#FFFFFF',
      textShadowRadius:10,
      fontSize: 20,
      fontWeight:'bold',
      fontFamily:'karla',
      textAlign:'center'
    }
  },
  {
    key: 'somethun1',
    title: 'Custom Itinerary',
    text: 'Built your custom itinerary based on your Schedule, Priorities and hyper-personalization  ',
    backgroundColor: '#96E',
    image :require( '../../assets/images/slider/slide3.png'),
    justifyContent: 'space-around',
    titleStyle : {
      color:'#ffffff',
      fontSize: 50,
      fontWeight:'bold',
      textShadowColor:'#FFFFFF',
      textShadowRadius:30,
      fontFamily:'karla',
      textAlign:'center',
    },
    textStyle:{
      color:'#ffffff',
      textShadowColor:'#FFFFFF',
      textShadowRadius:10,
      fontSize: 20,
      fontWeight:'bold',
      fontFamily:'karla',
      textAlign:'center',
    }
  },
  {
    key: 'somethun2',
    title: 'Indoor Navigation',
    text: 'Get help in navigating through the \n numerous attractions inside a monument.\n  ',
    backgroundColor: '#ff8da1',
    image :require( '../../assets/images/slider/slide4.png'),
    justifyContent: 'space-around',
    titleStyle : {
      color:'#ffffff',
      fontSize: 50,
      fontWeight:'bold',
      textShadowColor:'#FFFFFF',
      textShadowRadius:30,
      fontFamily:'karla',
      textAlign:'center',
    },
    textStyle:{
      color:'#ffffff',
      textShadowColor:'#FFFFFF',
      textShadowRadius:10,
      fontSize: 20,
      fontWeight:'bold',
      fontFamily:'karla',
      textAlign:'center'
    }
  }
];
 
export default class IntroSlider extends React.Component {
  constructor(props) {
    super(props);
    
  }

  static contextType = FtimeContext;
  componentDidMount() {
    const auth = this.context;
    console.log(auth);
  }

  _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Ionicons
          name="md-arrow-round-forward"
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{ backgroundColor: 'transparent' }}
        />
      </View>
    );
  }
  _renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle} backgroundColor="rgba(0, 255, 0, .9)"
      
      >
        <TouchableOpacity onPress={()=>{this._onDone()}}>
        <Ionicons
          name="md-checkmark"
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{ backgroundColor: 'transparent' }}
        />
        </TouchableOpacity>
      </View>
    );
  };
  _onDone = () => {
    Vibration.vibrate(50);
    this.context.donex();
    // AsyncStorage.setItem('isFirstTime', 'false');
    
    // this.props.navigation.navigate('nftime');
  };

  _onSkip = () => {
    this._onDone();
  };

  render() {
    // if (this.state.loading) return <ActivityIndicator size="large" />

    return (
      <AppIntroSlider
        slides={slides}
        showSkipButton={true}
        onSkip={this._onSkip}
        onDone = {this._onDone}
        renderDoneButton={this._renderDoneButton}
        renderNextButton={this._renderNextButton}
        renderItem={this._renderItem}
        paginationStyle={styles.pagestyle}
      />
    );
  }
}