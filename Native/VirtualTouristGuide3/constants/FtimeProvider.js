import React, { useState } from 'react';
import {AsyncStorage } from 'react-native';


export const FtimeContext =  React.createContext({
    ftimex: true,
    donex:()=> {},
    remove:()=>{}
});

export const FtimeProvider = ({
    children
}) => {
    const [ftimex, setftimex] = useState(true);
    return(
    <FtimeContext.Provider
     value={{
        ftimex,
        donex:() =>{
            setftimex(false);
            AsyncStorage.setItem('isFirstTime','false');
        },
        remove:() =>{
            AsyncStorage.removeItem('isFirstTime');
            setftimex(true);
        }
    }}>
        {children}
    </FtimeContext.Provider>
    );
}


