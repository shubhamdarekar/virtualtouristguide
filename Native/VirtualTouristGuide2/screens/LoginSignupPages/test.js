import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View} from 'react-native';
import Logo from './Logo';
import Form from './Form';
import Wallpaper from './Wallpaper';
import ButtonSubmit from './ButtonSubmit';
import SignupSection from './SignupSection';
import {
  Scene,
  Router,
  Actions,
} from 'react-native-router-flux';
import FirstScreen from '../FirstScreen';

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
          uname   : '',
          pass: '',
        };      
    this.callBack1 = this.callBack1.bind(this);
    this.callBack2 = this.callBack2.bind(this);
      }

      callBack1(uname){
        this.setState({uname});
      }
      callBack2(pass){
        this.setState({pass});
      }

  render() {
    return (
      <Wallpaper>
        <Logo />
        <Form
        uname = {this.callBack1}
        pass = {this.callBack2}
        />
        <SignupSection/>
        <ButtonSubmit
        uname = {this.state.uname}
        pass = {this.state.pass}/>
      </Wallpaper>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
export default class LoginScreen extends Component {
  
  render() {
    return (
      <View style={styles.container}>
          {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
          <Router>
          <Scene>
          <Scene key="main" component={Main} />
          <Scene key="home" component={FirstScreen}  />
          </Scene>
        </Router>
          </View>
        
    );
  }
}