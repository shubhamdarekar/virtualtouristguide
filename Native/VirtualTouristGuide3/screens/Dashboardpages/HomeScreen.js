import React from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ImageBackground,
    Image,
    TouchableOpacity, 
    Icon 
}
     from 'react-native';

const HomeScreen = ({
    params,
}) => (
    <ImageBackground source={require('../../assets/images/homeScreen/bgImage.png')} style={styles.bgImage}>
      <Image source={require('../../assets/images/homeScreen/icon.png')} style={styles.title} />
      <View style={styles.iconLayer}>
        <View style={styles.iconInner}>
          <TouchableOpacity style={styles.button}>
            <Image source={require('../../assets/images/homeScreen/Itinerary.png')} />
          </TouchableOpacity>
          <Text style={{textAlign: 'center', color:'#0001FC'}}>Itinerary</Text>
        </View>
        <View style={styles.iconInner}>
          <TouchableOpacity style={styles.button}>
            <Image source={require('../../assets/images/homeScreen/Rating.png')}/>
          </TouchableOpacity>
          <Text style={{textAlign: 'center', color:'#0001FC'}}>Rating</Text>
        </View>
        <View style={styles.iconInner}>
          <TouchableOpacity style={styles.button}>
            <Image source={require('../../assets/images/homeScreen/Suggestion.png')}/>
          </TouchableOpacity>
          <Text style={{textAlign: 'center', color:'#0001FC'}}>Suggestion</Text>
        </View>
        <View style={styles.iconInner}>
          <TouchableOpacity style={styles.button}>
            <Image source={require('../../assets/images/homeScreen/Chat.png')}/>
          </TouchableOpacity>
          <Text style={{textAlign: 'center', color:'#0001FC'}}>Forum</Text>
        </View>
      </View>
      <Text style={{fontFamily:'karla', fontSize:30, top:'7%', color: '#0A1034', fontWeight: 'bold'}}>
        Top Features
      </Text>
      <View style={styles.featureLayer}>
        <TouchableOpacity style={styles.featureInner}>
          <Image source={require('../../assets/images/homeScreen/capture.png')} style={{top:'20%'}} />
          <Text style={{top:'30%', fontSize:20, fontWeight: 'bold'}}>Capture</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.featureInner}>
          <Image source={require('../../assets/images/homeScreen/indoor.png')} style={{top:'20%'}} /> 
          <Text style={{top:'30%', fontSize:20, fontWeight: 'bold'}}>Indoor</Text>
          <Text style={{top:'30%', fontSize:20, fontWeight: 'bold'}}>Navigation</Text>
        </TouchableOpacity>
      </View>
  </ImageBackground>
);



const styles = StyleSheet.create({
    bgImage: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    title: {
      position: 'absolute',
      top: '6%',
      height: 120,
      width: '95%',
    },
    iconLayer: {
      top: '10%',
      flexDirection: 'row',
    },
    iconInner:{
      padding:10,
      textAlign: 'center',
    },
    button: {
      borderWidth:1,
      borderColor:'rgba(0,0,0,0.2)',
      alignItems:'center',
      justifyContent:'center',
      width:60,
      height:60,
      backgroundColor:'#fff',
      borderRadius:30,
    },
    featureLayer:{
      top:'20%',
      flexDirection: 'row',
    },
    featureInner:{
      width:'42%',
      height: 250,
      backgroundColor: 'rgba(255,255,255,.7)',
      marginLeft: '2.5%',
      marginRight: '2.5%',
      borderRadius: 20,
      alignItems: 'center'
    }
  });  

export default HomeScreen;
