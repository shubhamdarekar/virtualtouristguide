from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer

# Creating chatbot instance object
english_bot = ChatBot("Chatterbot", storage_adapter="chatterbot.storage.SQLStorageAdapter")
trainer = ChatterBotCorpusTrainer(english_bot)
trainer.train("chatterbot.corpus.english")
