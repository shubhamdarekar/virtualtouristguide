from django.conf.urls import url

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path

from customauth import views

urlpatterns = [
    url('login/', views.login_view, name='login'),
    url('^logout/$', views.logout_view, name='logout'),
    url('checkMobile/$', views.authRequest, name='checkMobile')
    # url('^check/$', views.check_view, name='check'),

]
