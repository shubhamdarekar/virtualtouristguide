import React, { useState } from 'react';
import {AsyncStorage, Vibration,ToastAndroid } from 'react-native';
import {serverUrl} from './url'


export const AuthContext =  React.createContext({
    user:null,
    done:false,
    login:(number,password)=> {},
    logout:()=>{},
});

export const AuthProvider = ({
    children
}) => {
    const [user,setUser] = useState(null);
    const [done,setDone] = useState(false);
    return(
    <AuthContext.Provider
     value={{
        user,
        login:(number,password) =>{
            setDone(false);
            ToastAndroid.show('Trying to log you in....', ToastAndroid.LONG);
            fetch(serverUrl.toString() + '/login/', {
                method: 'POST',
                headers: new Headers({
                  'Content-Type': 'application/x-www-form-urlencoded',
                }),
                body: 'mobile=' + number + '&pwd=' + password,
              }).then((response) => response.json())
                .then((responseJson) => {
          
                  console.log(responseJson);
                  if (responseJson.msg === "success") {


                    const SampleUser = { number: number,session_id: responseJson.session_id,password:password,pImage:null};
                    setUser(SampleUser);
                    AsyncStorage.setItem('user',JSON.stringify(SampleUser));
                    ToastAndroid.show('Welcome', ToastAndroid.LONG);
                    setDone(true);


                  }
                  else if (responseJson.msg === 'error') {


                    Vibration.vibrate(150);
                    ToastAndroid.show('The Password is Wrong.\nYou might have changed it recently.', ToastAndroid.SHORT);
                    setUser(null);
                    AsyncStorage.removeItem('user');
                    setDone(true);
                  }
          
                }).catch((error) => {


                  Vibration.vibrate(150);
                  console.log(error + "Server Error or No Internet Connection");
                    ToastAndroid.show('Server Error or No Internet Connection', ToastAndroid.SHORT);
                    setDone(true);


                });
            
        },
        logout: ()=>{
          setDone(false);
            setUser(null);
            AsyncStorage.removeItem('user');
            setDone(true);
        },
        done
    }}>
        {children}
    </AuthContext.Provider>
    );
}


