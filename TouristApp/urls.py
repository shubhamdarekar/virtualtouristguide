from django.conf.urls import url

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path

from TouristApp import views

urlpatterns = [
    url('chatbot/getresponse/', views.get_bot_response, name='chatbot-response'),
    url('chatforum/getdata/', views.get_chatforum_data, name='chatforum-get-data'),
    url('chatforum/adddata/', views.add_chatforum_data, name='chatforum-add-data'),
    url('addimage/', views.get_image_label, name='addimage-info'),
    url('visitors/getdata/', views.get_visitors_data, name='get-visitors-data'),
    url('^$', views.index)
]
