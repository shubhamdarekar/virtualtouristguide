from django.contrib.auth import login, authenticate, logout
from django.http import HttpResponse
from django.http import JsonResponse
from .models import User
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
# Create your views here.
def login_view(request):
    if request.method == "POST":
        mobile = request.POST.get('mobile')
        password = request.POST.get('pwd')
        # user = User.objects.get(mobile='7045507826')
        user = authenticate(request, mobile=mobile, password=password)
        if user is not None:
            login(request, user)
            # Redirect to a success page.
            # if(user.password == password):
            request.session['user_id'] = user.id
            loginSuccess = {
                "msg": "success",
                "session_id": str(request.session['user_id'])
            }
            return JsonResponse(loginSuccess)
        else:
            # Return an 'invalid login' error message.
            loginFailure = {
                "msg": "error",
                "mobile": mobile,
                "password": password
            }
            return JsonResponse(loginFailure)


def logout_view(request):
    try:
        del request.session['user_id']
    except KeyError:
        pass

    logout(request)
    logoutSuccess = {
        "msg": "Logout succesful"
    }
    return JsonResponse(logoutSuccess)


# def check_view(request):
# return HttpResponse(request.user.is_authenticated)

@csrf_exempt
def authRequest(request):
    # function to authenticate each request made to django urls
    if request.method == "POST":
        code = request.POST.get('mobile')[:2]
        mobile = request.POST.get('mobile')[2:]
        if User.objects.filter(mobile=mobile, countrycode=code).exists():
            responseText = {
                'msg': "success",
                'new': 'false',
                'mobile': mobile
            }
        else:
            responseText = {
                'msg': "success",
                'new': 'true',
                'mobile': mobile
            }
    else:
        responseText = {
            'msg': 'error'
        }
    return JsonResponse(responseText)


def username_present(request):
    if request.method == "POST":
        mobile = request.POST.get('mobile');
        if User.objects.filter(mobile=mobile).exists():
            return JsonResponse({'msg': 'present'})

    return JsonResponse({'msg': 'absent'})
