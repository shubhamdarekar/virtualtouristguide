from django.apps import AppConfig


class TouristappConfig(AppConfig):
    name = 'TouristApp'
