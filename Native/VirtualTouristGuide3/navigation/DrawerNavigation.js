import React,{useContext,useState} from 'react';
import { AuthContext } from '../constants/AuthProvider';
import TestPage from '../screens/test';
import { createDrawerNavigator ,
  DrawerItem,
  DrawerContentScrollView,
  DrawerItemList
} from '@react-navigation/drawer';
import {View,Text,StyleSheet, Alert,Image} from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import AwesomeAlert from 'react-native-awesome-alerts';
import { FloatingAction } from "react-native-floating-action";

//For Drawer Content
import {
  Avatar,
  Title,
  Caption,
  Paragraph,
  Drawer,
  // Text,
  TouchableRipple,
  Switch,
  Portal,
} from 'react-native-paper';
import HomeScreen from '../screens/Dashboardpages/HomeScreen';



const DrawerVariable = createDrawerNavigator();

function DrawerContent(props) {
  const { user,logout } = useContext(AuthContext);
  const [logoutBox, setlogoutBox] = useState(false);


    return (
      <DrawerContentScrollView {...props}>
      <View
        style={
          styles.drawerContent
        }
      >
        <View style={styles.userInfoSection}>
          {user.pImage ?
          <Avatar.Image
            source={{
              uri:
                'https://pbs.twimg.com/profile_images/952545910990495744/b59hSXUd_400x400.jpg',
            }}
            size={50}
          /> :
          <Avatar.Text size = {64} label = {user.number.slice(-4)}/>
          }
          <Title style={styles.title}>{user.number}</Title>
          <Caption style={styles.caption}>@{user.password}</Caption>
          <View style={styles.row}>
            <View style={styles.section}>
              <Paragraph style={[styles.paragraph, styles.caption]}>
                x
              </Paragraph>
              <Caption style={styles.caption}>Places Visited</Caption>
            </View>
          </View>
        </View>
        <Drawer.Section style={styles.drawerSection}>
        <DrawerItemList {...props} />
        </Drawer.Section>
        <Drawer.Section title="Preferences">
          <TouchableRipple onPress={() => {}}>
            <View style={styles.preference}>
              <Text>Dark Theme</Text>
              <View pointerEvents="none">
                <Switch value={false} />
              </View>
            </View>
          </TouchableRipple>
          <TouchableRipple onPress={()=>{
            setlogoutBox(true);
            props.navigation.closeDrawer();
            }}>
          <View style={styles.preference}>
            <Text>Not {user.number}? Logout</Text>
            </View>
          </TouchableRipple>
        </Drawer.Section>
      </View>
      <Portal>
      <AwesomeAlert
          show={logoutBox}
          showProgress={false}
          title="Logout"
          message="Do you really want to logout?"
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={true}
          showCancelButton={true}
          showConfirmButton={true}
          cancelText="No Just kidding."
          confirmText="Yes!!"
          confirmButtonColor="#03fc5a"
          contentContainerStyle ={{
            height:160,
            width:250,
            justifyContent:'center',
          }}
          onDismiss = {()=>{
            props.navigation.openDrawer();
            setlogoutBox(false);
          }}
          onCancelPressed={() => {
            setlogoutBox(false);
          }}
          onConfirmPressed={() => {
            logout();
            setlogoutBox(false);
          }}
        />
      </Portal>
    </DrawerContentScrollView>
    );
  }


const AppStack = () => {
    return (
      <React.Fragment>
        <DrawerVariable.Navigator
         initialRouteName = 'Home'
         drawerContent={(props) => <DrawerContent {...props}
         backBehavior = 'initialRoute'
         drawerContentOptions={{
          activeTintColor: '#d0d055',
         }}
         hideStatusBar
         drawerPosition={"right"}
         />
         }>
            <DrawerVariable.Screen 
            name="Home" 
            component={HomeScreen} 
            options ={{drawerIcon:(color,size)=>{return <MaterialCommunityIcons
                name="home"
                color={color.color}
                size={25}
              />},
            }} />
            {/* <DrawerVariable.Screen name="Logout" component={LogoutPage} /> */}
            <DrawerVariable.Screen 
            name="Settings" 
            component={TestPage} 
            options ={{drawerIcon:(color,size)=>{return <MaterialCommunityIcons
                name="tune"
                color={color.color}
                size={25}
              />}}} />
            <DrawerVariable.Screen 
            name="Links" 
            component={TestPage} 
            options ={{drawerIcon:(color,size)=>{return <MaterialCommunityIcons
                name="bookmark-outline"
                color={color.color}
                size={25}
              />}}} />
            <DrawerVariable.Screen 
            name="Nearby Places" 
            component={TestPage} 
            options ={{drawerIcon:(color,size)=>{return <MaterialCommunityIcons
                name="map-marker"
                color={color.color}
                size={25}
              />}}} />
            <DrawerVariable.Screen 
            name="Form" 
            component={TestPage} 
            options ={{drawerIcon:(color,size)=>{return <MaterialCommunityIcons
                name="format-list-numbered"
                color={color.color}
                size={25}
              />}}} />
        </DrawerVariable.Navigator>


        <Portal>
          <FloatingAction
            actions={[{
              text: "Chatbot",
              icon: { uri: "https://cdn3.iconfinder.com/data/icons/google-material-design-icons/48/ic_message_48px-512.png" },
              name: "Chatbot",
              position: 1,
              buttonSize: 60,
              color: 'rgba(255,255,255,0)',
              textBackground: 'rgba(255,255,255,0.2)',
              textElevation: 0,
              textStyle: { fontSize: 20, color: 'black', fontWeight: 'bold', shadowColor: 'white' },
            },
            {
              text: "Capture",
              icon: { uri: "https://www.stickpng.com/assets/images/584abe1a2912007028bd932e.png" },
              name: "Capture",
              position: 2,
              buttonSize: 60,
              color: 'rgba(255,255,255,0)',
              textBackground: 'rgba(255,255,255,0.2)',
              textElevation: 0,
              textStyle: { fontSize: 20, color: 'black', fontWeight: 'bold', shadowColor: 'white' },
            }
            ]}
            onPressItem={name => {
              console.log(`selected button:`);
            }}
            listenKeyboard={true}
            shadow={
              { shadowOpacity: 1, shadowOffset: { width: 0, height: 5 }, shadowColor: "#000000", shadowRadius: 3 }
            }
          />
        </Portal>


        </React.Fragment>
    )
}

export default AppStack;


const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 20,
  },
  title: {
    marginTop: 20,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  row: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15,
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
  },
  preference: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});