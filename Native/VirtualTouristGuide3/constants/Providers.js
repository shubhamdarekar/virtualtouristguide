import React from 'react';
import { AuthProvider } from './AuthProvider';
import MainNavigation from '../navigation/mainNavigation';
import { FtimeProvider } from './FtimeProvider';
import { Provider as PaperProvider } from 'react-native-paper';

const Providers = ({
}) => (
    <PaperProvider>
    <AuthProvider>
        <FtimeProvider>
            <MainNavigation />
        </FtimeProvider>
    </AuthProvider>
    </PaperProvider>
);

export default Providers;
