import React from 'react';
import { Ionicons } from '@expo/vector-icons';
import { StyleSheet, View, TouchableOpacity, AsyncStorage,ActivityIndicator,Text,ImageBackground,Image ,Button} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';

const styles = StyleSheet.create({
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    
  },
  image: {
    width: 320,
    height: 320,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  welcome: {
    position: 'absolute',
    top: '12%',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 23,
    lineHeight: 24,
    color: '#FFFFFF'
  },
  travelr: {
    position: 'absolute',
    top: '18%',
    textAlign: 'center',
    flexDirection: 'row',
  },
  travelr1: {
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 50,
    // fontFamily: 'Pacifico-Regular',
    lineHeight: 50,
    textAlign: 'center',
    color: '#000000',
  },
  travelr2: {
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 50,
    // fontFamily: 'Pacifico-Regular',
    lineHeight: 50,
    textAlign: 'center',
    color: '#FFFFFF',
  },
  image: {
    position: 'absolute',
    top: '33%'
  },
  start: {
    position: 'absolute',
    top: '61.64%',
    // fontFamily: 'Karla',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 24,
    lineHeight: 24,
    textAlign: 'center',
    color: '#000000',
  },
  start2: {
    position: 'absolute',
    top: '67.64%',
  },
  start3: {
    // fontFamily: 'Karla',
    fontStyle: 'normal',
    fontSize: 18,
    lineHeight: 20,
    textAlign: 'center',
    color: '#000000',
  },
  next: {
    position: 'absolute',
    top: '86%',
    width: '40%',
  }

});

const slides = [
  {
    key: 'somethun',
    title: ' Welcome to \n    TRAVELr',
    text: "Let's Get Started  \n\nTake control of your travel \nby tracking and constantly updating\n your itinerary, ",
    textStyle: TextStyle(fontWeight: FontWeight.bold),
    backgroundColor: '#F58B8B',
    image :require( './assets/slider/goa.png'),
    justifyContent: 'space-around'
  },
  {
    key: 'somethun-dos',
    title: 'Instant Information',
    text: 'Get instant information about your place \nyou are visiting just by clicking a picture',
    backgroundColor: '#febe29',
    image :require( './assets/slider/slide2.png'),
    justifyContent: 'space-around'
  },
  {
    key: 'somethun1',
    title: 'Custom Itinerary',
    text: 'Built your custom itinerary based on your Schedule, Priorities and hyper-personalization  ',
    backgroundColor: '#96E',
    image :require( './assets/slider/slide3.png'),
    justifyContent: 'space-around'
  }
];

const slides2 = [
  {
    key:'one',
    utext:'Welcome To',
    bimage:require('./assets/slider/Rectangle.png'),
    utext:'Welcome to',
    title:'Travel',
    title2:'r',
    image:require('./assets/slider/Rectangle.png'),
    dtext:'Take control of your travel \nby tracking and constantly updating\n your itinerary,  ',
    ctext :'Lets Get Started',
  },
  {
    key:'two',
    utext:'Welcome To',
    // bimage:require('./assets/slider/Rectangle.png'),
    utext:'Welcome to',
    title:'Travel',
    title2:'r',
    image:require('./assets/slider/Rectangle.png'),
    dtext:'Take control of your travel \nby tracking and constantly updating\n your itinerary,  ',
    ctext :'Lets Get Started',
  },
  {
    key:'three',
    utext:'Welcome To',
    // bimage:require('./assets/slider/Rectangle.png'),
    utext:'Welcome to',
    title:'Travel',
    title2:'r',
    image:require('./assets/slider/Rectangle.png'),
    dtext:'Take control of your travel \nby tracking and constantly updating\n your itinerary,  ',
    ctext :'Lets Get Started',
  }
];
 
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showRealApp: false,
      loading: true,
      //To show the main page of the app
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('first_time').then((value) => {
      this.setState({ showRealApp: !!value, loading: false });
    });
  }

  _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Ionicons
          name="md-arrow-round-forward"
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{ backgroundColor: 'transparent' }}
        />
      </View>
    );
  };
  _renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle} 
      >
        <TouchableOpacity onPress={()=>{this._onDone()}}>
        <Ionicons
          name="md-checkmark"
          color="rgba(255, 255, 255, .9)"
          size={24}
          style={{ backgroundColor: 'transparent' }}
        />
        </TouchableOpacity>
      </View>
    );
  };
  _onDone = () => {
    AsyncStorage.setItem('first_time', 'true').then(() => {
      this.setState({ showRealApp: true });
    });
  }

  _onSkip = () => {
    AsyncStorage.setItem('first_time', 'true').then(() => {
      this.setState({ showRealApp: true });
    });
  };

  render() {
    if (this.state.loading) return <ActivityIndicator size="large" />
    
    if (this.state.showRealApp) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            padding: 50,
          }}>
          <Text>
            This will be your screen when you click Skip from any slide or Done
            button at last
          </Text>
        </View>
      );
    } else {

    return (
      <AppIntroSlider
        slides={slides2}
        showSkipButton={true}
        onSkip={this._onSkip}
        renderDoneButton={this._renderDoneButton}
        renderNextButton={this._renderNextButton}
        // renderItem={this._renderItem}
      />
    );
    }
  }
}