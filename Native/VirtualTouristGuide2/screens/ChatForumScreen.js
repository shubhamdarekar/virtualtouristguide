import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  TextInput,
  FlatList,
  Button,
  KeyboardAvoidingView,
} from 'react-native';
// import {Headers} from 'react-navigation-stack'
export default class Chat extends Component {

  constructor(props) {
    super(props);
    this.state = {
      msg: '',
      data: [
      ],
      uid : 2,
      name_address : ''
    };

    fetch('http://192.168.137.158:8000/app/chatforum/getdata/', {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded',
      }),
      body:'',
    }).then((response) => response.json())
    .then((responseJson) => {
      var a=[]
      console.log(responseJson);
      for(i=0;i<responseJson.length;i++){
        responseJson[i]['type'] = "out";
        a.push(responseJson[i])
      }
      this.setState({data:a});
      console.log(this.state.data);
      
      

    });

  }
  _press = () =>{
    console.log("in");
    fetch('http://192.168.137.158:8000/app/chatforum/adddata/', {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded',
      }),
      body:'msg='+this.state.name_address+'&u_id='+this.state.uid,
    }).then((response) => response.json())
    .then((responseJson) => {
      var a=[]
      console.log(responseJson);
      for(i=0;i<responseJson.length;i++){
        responseJson[i]['type'] = "out";
        a.push(responseJson[i])
      }
      this.setState({data:a});
      console.log(this.state.data);
      
      

    });
  }

  _press(){
    let z = this.state.i;
    z += 1;
    console.log(z)
    this.setState({i: z})
    console.log(this.state.msg);
    let x = {id: this.state.i, date: "9:50 pm", type: "out", message: this.state.msg};
    this.state.data.push(x)
    this.setState({data: this.state.data})
    console.log(this.state.data)
        fetch('http://192.168.1.8:8000/app/chatforum/adddata/', {
      method: 'POST',
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded',
      }),
      body:'msg='+this.state.msg,
    }).then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson);
      let num = this.state.i;
      num += 1;
      this.setState({i: num})
      let res = {id: this.state.i, date: "9:50 pm", type: "in", message: responseJson.res};
      this.state.data.push(res)
      this.setState({data: this.state.data})
    });
  }

  renderDate = (date) => {
    return(
      <Text style={styles.time}>
        {date}
      </Text>
    );
  }

  render() {

    return (
      <View style={styles.container}>
        <FlatList style={styles.list}
          data={this.state.data}
          keyExtractor= {(item) => {
            return item.id;
          }}
          renderItem={(message) => {
            console.log(item);
            const item = message.item;
            let inMessage = item.type === 'in';
            let itemStyle = inMessage ? styles.itemIn : styles.itemOut;
            return (
              <View style={[styles.item, itemStyle]}>
                {!inMessage && this.renderDate(item.date)}
                <View style={[styles.balloon]}>
                  <Text>{item.message}</Text>
                </View>
                {inMessage && this.renderDate(item.date)}
              </View>
            )
          }}/>
        <KeyboardAvoidingView behavior="padding">
        <View style={styles.footer}>
          <View style={styles.inputContainer}>
            <TextInput style={styles.inputs}
                placeholder="Write a message..."
                underlineColorAndroid='transparent'
                onChangeText={(msg) => this.setState({msg})}
                blurOnSubmit = {true}
            />
          </View>

            <TouchableOpacity style={styles.btnSend} onPress={this._press.bind(this)}>
              <Image source={{uri:"https://png.icons8.com/small/75/ffffff/filled-sent.png"}} style={styles.iconSend}  />
            </TouchableOpacity>
        </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1
  },
  list:{
    paddingHorizontal: 17,
  },
  footer:{
    flexDirection: 'row',
    height:60,
    backgroundColor: '#eeeeee',
    paddingHorizontal:10,
    padding:5,
  },
  btnSend:{
    backgroundColor:"#00BFFF",
    width:40,
    height:40,
    borderRadius:360,
    alignItems:'center',
    justifyContent:'center',
  },
  iconSend:{
    width:30,
    height:30,
    alignSelf:'center',
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius:30,
    borderBottomWidth: 1,
    height:40,
    flexDirection: 'row',
    alignItems:'center',
    flex:1,
    marginRight:10,
  },
  inputs:{
    height:40,
    marginLeft:16,
    borderBottomColor: '#FFFFFF',
    flex:1,
  },
  balloon: {
    maxWidth: 250,
    padding: 15,
    borderRadius: 20,
  },
  itemIn: {
    alignSelf: 'flex-start'
  },
  itemOut: {
    alignSelf: 'flex-end'
  },
  time: {
    alignSelf: 'flex-end',
    margin: 15,
    fontSize:12,
    color:"#808080",
  },
  item: {
    marginVertical: 14,
    flex: 1,
    flexDirection: 'row',
    backgroundColor:"#eeeeee",
    borderRadius:300,
    padding:5,
  },
});