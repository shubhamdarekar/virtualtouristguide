import React, { useEffect, useState, useContext } from 'react';
import { AsyncStorage,View } from 'react-native';
import AnimatedLoader from 'react-native-animated-loader';
import LoginView from '../screens/loginSignupScreens/LoginPage';
import IntroSlider from '../screens/introScreens/introSlider';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { AuthContext } from '../constants/AuthProvider';
import { FtimeContext } from '../constants/FtimeProvider';
import EnterPassword from '../screens/loginSignupScreens/EnterPassword';
import AppStack from './DrawerNavigation';

const Stack = createStackNavigator();



const FtimeStack = () => {
    return (
        <Stack.Navigator
            screenOptions={{
                headerMode: 'none',
                header: () => null,
                mode: 'card',
                ...TransitionPresets.SlideFromRightIOS,
                gestureEnabled: true,
                gestureDirection: 'horizontal',
            }}
        >
            <Stack.Screen name="Intro" component={IntroSlider} />
        </Stack.Navigator>
    )
}

const LoginSignupStack = () => {
    return (
        <Stack.Navigator
            screenOptions={{
                headerMode: 'none',
                header: () => null,
                mode: 'card',
                ...TransitionPresets.SlideFromRightIOS,
                gestureEnabled: true,
                gestureDirection: 'horizontal',
            }}
        >
            <Stack.Screen
                name="Enter Mobile"
                component={LoginView}
            />
            <Stack.Screen name="Password" component={EnterPassword} />
        </Stack.Navigator>
    )
}



const MainNavigation = ({ }) => {
    const { user, login, logout,done } = useContext(AuthContext);
    const { ftimex, donex } = useContext(FtimeContext);
    const [ loading1, setloading1] = useState(false);


    useEffect(() => {
        AsyncStorage.getItem('isFirstTime').then((value) => {
            if (value == 'false') {
                donex();
            } else {
            }
            setloading1(true);
        }).catch(err => {
            console.log(err);
        });

        AsyncStorage.getItem('user').then(userString => {
            userString = JSON.parse(userString);
            if (userString != null) {
                login(userString.number, userString.password);
            }
            else {
                logout();
            }

        }
        ).catch(err => {
            console.log(err);
        });

    }, [])
    return (
        <View style ={{flex: 1,padding: 0,}}>
        <AnimatedLoader 
        visible={!loading1 || !done}
        overlayColor="rgba(255,255,255,1)"
        animationStyle={{width: 100,    height: 100,}}          
        speed={1}
        source={require("../assets/loadinganimation.json")}
        />
        <NavigationContainer>
            {ftimex ? (<FtimeStack />) : (user ? (<AppStack />) : (<LoginSignupStack />))}
        </NavigationContainer>
        </View>
    );
}

export default MainNavigation;