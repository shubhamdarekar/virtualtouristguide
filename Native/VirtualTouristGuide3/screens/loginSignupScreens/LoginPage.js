import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  ImageBackground,
  Image,
  TextInput,
  Dimensions,
  KeyboardAvoidingView,
  Vibration,
  ToastAndroid,
  Keyboard,
  TouchableOpacity
} from 'react-native';
import { serverUrl } from '../../constants/url';
import AnimatedLoader from 'react-native-animated-loader';
import { FtimeContext } from '../../constants/FtimeProvider';


// or any pure javascript modules available in npm
import CountryPicker, { DARK_THEME } from 'react-native-country-picker-modal';
import AwesomeButtonRick from 'react-native-really-awesome-button/src/themes/rick';

import bgSrc from '../../assets/images/wallpaper.png';
import logoImg from '../../assets/images/logo.png';

// const AuthContext = React.createContext({
//   user:null,
//   login:()=>{},
//   logout:()=>{},
// });

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileNumber: '',
      countryCode: '91',
      selectedCountry: 'IN',
      fetching: false,
      tendigits: false,
    };
  }

  static contextType = FtimeContext;
  componentDidMount() {
    this.fftime = this.context;
  }

  render() {
    const { fetching } = this.state;
    return (
      <View style={styles.container}>
        <AnimatedLoader
          visible={fetching}
          overlayColor="rgba(255,255,255,0.75)"
          animationStyle={styles.lottie}
          speed={1}
          source={require("../../assets/loadinganimation.json")}
        />
        <ImageBackground style={styles.picture} source={bgSrc}>
          <View style={styles.logoview}>
            <Image source={logoImg} style={styles.logoimage} />
            <Text style={styles.nametext}>TRAVELr</Text>
          </View>
          <KeyboardAvoidingView behavior={'padding'} style={styles.centerWrapper}>
            <View style={styles.centerWrapper}>
              <View style={styles.inputWrapper}>
                <TextInput
                  style={styles.input}
                  maxLength={10}
                  placeholder={'Mobile Number'}
                  keyboardAppearance={'dark'}
                  autoCorrect={false}
                  autoCapitalize={'none'}
                  returnKeyType={'done'}
                  placeholderTextColor="white"
                  underlineColorAndroid="transparent"
                  autoFocus={true}
                  keyboardType={'phone-pad'}
                  dataDetectorTypes={'phoneNumber'}
                  onChangeText={text => {
                    this.setState({ mobileNumber: text });
                    if (text.length == 10) {
                      Keyboard.dismiss();
                      this.setState({ tendigits: true });
                    }
                    else {
                      this.setState({ tendigits: false });
                    }
                  }}
                  value={this.state.mobileNumber}
                />

                {/* <View style = {{backgroundColor:'red',height}}> */}
                <CountryPicker
                  withCloseButton={false}
                  withFilter
                  withEmoji
                  containerButtonStyle={styles.inlineCountryPicker}
                  onSelect={value => {
                    this.setState({ countryCode: value['callingCode'] });
                    this.setState({ selectedCountry: value['cca2'] });
                  }}
                  countryCode={this.state.selectedCountry}
                  withCallingCode
                  theme={DARK_THEME}
                  withCallingCodeButton
                />
                {/* </View> */}
                <View
                  style={{
                    borderLeftWidth: 1,
                    borderLeftColor: 'black',
                    marginHorizontal: (DEVICE_WIDTH - 350) / 2 + 105,
                    height: 30,
                    top: 10,
                    position: 'absolute',
                  }}
                />
              </View>

              <View style={styles.nextWrapper}>
                <AwesomeButtonRick
                  type="secondary"
                  progress
                  width={150}
                  raiseLevel={10}
                  disabled={!this.state.tendigits}
                  onPress={next => {
                    this.setState({ fetching: true })
                    // to log in write this
                    // this.context.login("","");

                    fetch(serverUrl.toString() + '/checkMobile/', {
                      method: 'POST',
                      headers: new Headers({
                        'Content-Type': 'application/x-www-form-urlencoded',
                      }),
                      body: 'mobile=' + this.state.countryCode + this.state.mobileNumber,
                    }).then((response) => response.json())
                      .then((responseJson) => {
                        if (responseJson.new == "false") {
                          //ask for Password
                          this.setState({
                            fetching: false
                          })
                          Vibration.vibrate(50);
                          ToastAndroid.show("Returning User", ToastAndroid.SHORT);
                          this.props.navigation.navigate("Password", { number: responseJson.mobile });
                        } else {
                          this.setState({
                            fetching: false
                          })
                          Vibration.vibrate(50);
                          ToastAndroid.show("New User", ToastAndroid.SHORT);
                          //go to signup
                        }
                      }).catch((error) => {
                        this.setState({
                          fetching: false
                        })
                        Vibration.vibrate(150);
                        console.log(error + "Server Error or No Internet Connection");
                        ToastAndroid.show('Server Error or No Internet Connection', ToastAndroid.SHORT);
                      });
                    // console.log(this.state.mobileNumber);
                    next();
                    // this.setState({mobileNumber:''})
                  }}
                >
                  Next
              </AwesomeButtonRick>
              </View>
            </View>
          </KeyboardAvoidingView>
          <TouchableOpacity style={styles.seeIntroC}
            onPress={() => {
              this.context.remove();
            }}
          >
            <Text style={styles.seeIntroText}>See Intro Again</Text>
          </TouchableOpacity>
        </ImageBackground>
      </View>
    );
  }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 0,
  },
  picture: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: 'center',
    resizeMode: 'cover',
  },
  logoview: {
    top: DEVICE_HEIGHT / 10 - 20,
    flex: 0.7,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoimage: {
    width: 80,
    height: 80,
  },
  nametext: {
    color: 'white',
    backgroundColor: 'transparent',
    marginTop: 20,
    fontFamily: 'pacifico',
    fontSize: 30,
  },
  centerWrapper: {
    flex: 1,
    flexDirection: 'column',
  },
  inputWrapper: {
    top: 70,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  input: {
    backgroundColor: 'rgba(255, 255, 255, 0.6)',
    width: 350,
    height: 47,
    marginHorizontal: (DEVICE_WIDTH - 350) / 2,
    paddingLeft: 115,
    borderRadius: 30,
    color: '#ffffff',
    position: 'absolute',
  },
  inlineCountryPicker: {
    // backgroundColor:'red',
    top: 10,
    marginHorizontal: (DEVICE_WIDTH - 350) / 2 + 15,
  },
  nextWrapper: {
    top: 15,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  lottie: { width: 100, height: 100, },
  seeIntroC: {
    alignSelf: 'flex-end',
    padding: 15,
  },
  seeIntroText: {
    fontSize: 20,
    color: 'blue',
  },
});
